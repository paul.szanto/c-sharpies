﻿
namespace BilantzerForThePoor
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBilantzate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonBilantzate
            // 
            this.buttonBilantzate.Location = new System.Drawing.Point(25, 22);
            this.buttonBilantzate.Name = "buttonBilantzate";
            this.buttonBilantzate.Size = new System.Drawing.Size(211, 49);
            this.buttonBilantzate.TabIndex = 0;
            this.buttonBilantzate.Text = "BILANTZATE";
            this.buttonBilantzate.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 97);
            this.Controls.Add(this.buttonBilantzate);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BILANTZER";
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button buttonBilantzate;
    }
}