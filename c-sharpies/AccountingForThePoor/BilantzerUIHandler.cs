﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BilantzerForThePoor
{
    class BilantzerUIHandler
    {
        Dictionary<string, double> categoriesAndSums = new Dictionary<string, double>();
        OpenFileDialog fileDialogue;
        private MainWindow mainWindow;

        public BilantzerUIHandler(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            mainWindow.buttonBilantzate.Click += OnClickBilantzate;
            fileDialogue = new OpenFileDialog();
            fileDialogue.FileOk += OnSelectedFile;
            fileDialogue.DefaultExt = "csv";
            fileDialogue.Filter = "csv files (*.csv)|*.csv";
            fileDialogue.Title = "Select bilantz csv file";
        }

        private int CompareCatAndSum(CategoryAndSum x, CategoryAndSum y)
        {
            return x.cat.CompareTo(y.cat);
        }

        string CreateValueString(string tk1, string tk2)
        {
            tk1 = tk1.Replace(".", string.Empty);
            tk2 = tk2.Replace(".", string.Empty);
            return (tk1 + "." + tk2).Replace("\"", string.Empty);
        }

        private void OnClickBilantzate(object sender, EventArgs e)
        {
            fileDialogue.ShowDialog();
        }

        private void OnSelectedFile(object sender, CancelEventArgs e)
        {
            var dirPath = Path.GetDirectoryName(fileDialogue.FileName);

            var transactionCategories = JsonConvert.DeserializeObject<TransactionCategories>(File.ReadAllText(Path.Combine(dirPath, "..", "transaction_categories.json")));
            var reader = new StreamReader(fileDialogue.FileName);
            var line = reader.ReadLine();

            while (line != null)
            {
                var tokens = line.Split(',');
                if (tokens[0] != null && tokens[0] != string.Empty && tokens[0] != "Data")
                {
                    var transactionName = tokens[3];
                    string valueString;
                    if (tokens[4] != string.Empty && tokens[5] != string.Empty)
                    {
                        valueString = CreateValueString(tokens[4], tokens[5]);
                    }
                    else
                    {
                        valueString = CreateValueString(tokens[6], tokens[7]);
                    }
                    var transactionValue = double.Parse(valueString);

                    line = reader.ReadLine();
                    tokens = line.Split(',');
                    transactionName += "-" + tokens[3];

                    line = reader.ReadLine();
                    tokens = line.Split(',');
                    transactionName += "-" + tokens[3];
                    string category;
                    if (transactionCategories.dictionary.TryGetValue(transactionName, out category))
                    {
                        if (category == null || category == string.Empty)
                        {
                            MessageBox.Show($"Could not find category for transaction\n{transactionName}\nThe process will be halted until you get your life together");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show($"Could not find category for transaction\n{transactionName}\nThe process will be halted until you get your life together");
                        return;
                    }
                    if (categoriesAndSums.ContainsKey(category))
                    {
                        categoriesAndSums[category] += transactionValue;
                    }
                    else
                    {
                        categoriesAndSums.Add(category, transactionValue);
                    }
                    //Console.WriteLine($"{transactionName} {transactionValue.ToString("0.00")}");
                }
                line = reader.ReadLine();
            }
            reader.Close();

            MessageBox.Show("Done bilantzing... check the results in the results.csv file");

            string result = string.Empty;
            var catRow = string.Empty;
            var sumRow = string.Empty;
            var listOfCategoriesSorted = new List<CategoryAndSum>();
            foreach (var cat in categoriesAndSums)
            {
                listOfCategoriesSorted.Add(new CategoryAndSum
                {
                    cat = cat.Key,
                    total = cat.Value
                });
            }
            listOfCategoriesSorted.Sort(CompareCatAndSum);
            foreach (var catAndSum in listOfCategoriesSorted)
            {
                catRow += "," + catAndSum.cat;
                sumRow += "," + catAndSum.total;
            }

            File.WriteAllText(Path.Combine(dirPath, "results.csv"), catRow + "\n" + sumRow);
        }
    }

    class CategoryAndSum
    {
        public string cat;
        public double total;
    }

    class TransactionCategories
    {
        public Dictionary<string, string> dictionary = new Dictionary<string, string>();
    }
}