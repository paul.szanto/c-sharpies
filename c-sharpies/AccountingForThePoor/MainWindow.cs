﻿using System.Windows.Forms;

namespace BilantzerForThePoor
{
    public partial class MainWindow : Form
    {
        BilantzerUIHandler controller;

        public MainWindow()
        {
            InitializeComponent();
            controller = new BilantzerUIHandler(this);
        }
    }
}